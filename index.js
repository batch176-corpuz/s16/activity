let number = +prompt("Enter a number");
console.log(`The number you entered is ${number}.`);

for (number; number >= 0; number--) {
	if (number <= 50) {
		console.log(
			"Current value is less than or equal to 50, terminating loop."
		);
		break;
	} else if (number % 10 === 0) {
		console.log("Divisible by 10, skipping.");
		continue;
	} else if (number % 5 === 0) {
		console.log(number);
	}
}

let str = "supercalifragilisticexpialidocious";
let consonants = "";
for (let i = 0; i < str.length; i++) {
	str[i] == "a" ||
	str[i] == "e" ||
	str[i] == "o" ||
	str[i] == "i" ||
	str[i] == "u"
		? 1
		: (consonants += str[i]);
}

console.log(str);
console.log(consonants);
